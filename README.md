This repository contains a set of Ansible playbooks that can be used
to create a multi-node OpenShift cluster locally, on a workstation,
with libvirt managed VMs.

# Usage

## Prerequisites

The following packages are needed:

* `libvirt` and `qemu` (`libvirt`, `libvirt-nss` and `qemu-system-x86.x86_64` on Fedora)
* `ansible` (`ansible` and `libselinux-python` on Fedora)
* `libvirt-python` (`python2-libvirt` on Fedora)
* `python-lxml` (`python2-lxml` on Fedora)
* `libguestfs`

Then, check the runtime configuration of your host with `check_prerequisites`

```shell
./check_prerequisites
```

⚠ If you plan to put the directory containing the VM images
(`openshift_pool_path` parameter below) in your `HOME`, ensure that
qemu will be allowed to access that directory.

```shell
setfacl -m g:kvm:--x ~
```

or on Fedora:

```shell
setfacl -m u:qemu:--x ~
```

## Customize the cluster to create

All the parameters are in [config.yaml](config.yaml):

```shell
vim config.yaml
```

The parameters that can be customized are:

| Parameter | Description |
| --------- | ----------- |
| `openshift_pool_path` | The directory that will contain the drives of the VMs (i.e.: the qcow2 files) |
| `base_image_url`      | The URL of the base image to download                                         |
| `base_image_checksum` | The expected checksum of the base image to download                           |
| `etcd_count`          | The number of etcd nodes                                                      |
| `master_count`        | The number of masters                                                         |
| `node_count`          | The number of compute nodes                                                   |
| `infra_count`         | The number of infrastructure nodes                                            |
| `ssh_public_key`      | Your ssh public key                                                           |

## Create an OpenShift cluster

On your workstation:

```shell
ansible-playbook create.yaml
ssh bastion.openshift
```

On the bastion VM:

```shell
cd openshift-ansible/
ansible-playbook playbooks/prerequisites.yml
ansible nodes -m docker_image -a 'name=docker.io/openshift/origin-pod:v3.11.0 repository=docker.io/openshift/origin-pod:v3.11.0-rc.0'   # ⚠ Hack to be fixed
ansible-playbook playbooks/deploy_cluster.yml
```

## Delete all the instances but keep the base VM image

```shell
ansible-playbook destroy_instances.yaml
```

## Delete everything that was created by the `create.yaml` playbook

```shell
ansible-playbook destroy_all.yaml
```
